@extends('layouts.app')
<style>

    .yellow{
        background-color: yellow;
    }
    .white{
        background-color: #FFFFFF;
    }
</style>
@section('content')
<div class="container">
    <form id="my_form" method="post" action="{{action('FactureController@search')}}"></form>
    <form method="get" id="sort_total"action="{{action('FactureController@sortTotal')}}"></form>

    <div class="panel panel-primary filterable">
        <div class="panel-heading">
            <div class="pull-right">
                <button form="my_form" type ="submit" class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filtrer</button>
            </div>
        </div>
        <table id="facture_table" class="table">
            <thead>
            <div class="filters">
                <th><input type="checkbox" name="select-all" id="select-all"/></th>
                <th>
                    <div class="form-groupe">
                        <label for="numero">Numéro</label>
                        <input type="text" name="number" form="my_form" class="form-control" placeholder="numéro">
                    </div>
                </th>
                <th>Nom - Prénom</th>
                <th> Date</th>
                <th>
                    <label for="">Total:</label>
                    <div class="btn-group" role="group" aria-label="...">

                        <button type="submit" form="sort_total" name="sort" value="desc" class="btn btn-default"
                        onclick="">
                        <span id="sortDescIcon" class="pull-left" style="displey: inline-block;">
                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                        </span>

                        </button>
                        <button type="submit" form="sort_total" name="sort" value="asc"
                        class="btn btn-default">
                        <span id="sortAscIcon" class="pull-left" style="">
                        <i class="glyphicon glyphicon-sort-by-attributes-alt"></i>
                        </span>
                        </button>
                    </div>

                </th>

                <th><label for="statu">Status</label><input type="text" form="my_form" name="status"
                                                            class="form-control" placeholder="status"></th>
                <th>action</th>
                </tr>
            </div>
            </thead>
            <tbody id="tbody">
            <?php $yellow="yellow"; $white="white" ; ?>
            @foreach ($facture as $value)


                {{--<tr class="{{ $value->ligne_facture['quantity'] * $value->ligne_facture['unit_price'] < 100 ? $yellow : $white }}">--}}
                    {{--<th>--}}
                        {{--<input type="checkbox" name="checkbox-1" id="checkbox-1"></th>--}}
                    {{--<th scope="row">{{ $value->numero }}</th>--}}
                    {{--<td>{{ $value->lname.' '.$value->fname }}</td>--}}
                    {{--<td>{{ $value->date}}</td>--}}
                    {{--<td >{{number_format($value->total['total'])}} € </td>--}}

                    {{--<td>{{ $value->status }}</td>--}}
                    {{--<th><button class="delete btn btn-danger" data-fid="{{$value->id}}">delete</button></th>--}}
                {{--</tr>--}}

                <tr class="{{ $value->total < 100 ? $yellow : $white }}">
                    <th>
                        <input type="checkbox" name="checkbox-1" id="checkbox-1"></th>
                    <th scope="row">{{ $value->numero }}</th>
                    <td>{{ $value->lname.' '.$value->fname }}</td>
                    <td>{{ $value->date}}</td>
                    <td >{{number_format($value->total)}} € </td>

                    <td>{{ $value->status }}</td>
                    <th><button class="delete btn btn-danger" data-fid="{{$value->id}}">delete</button></th>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
    {!! $facture->appends(Input::except('page'))->render()!!}
</div>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>


<script>

    $(document).ready(function () {

    $('#select-all').click(function(event) {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });


    $('.delete').click(function(){
        var id = $(this).attr('data-fid');
        if(confirm("Etes vous sûr de vouloir supprimer?"))
        {
            $.ajax({
                url:"facture/delete/"+id,
                mehtod:"post",
                success:function(data)
                {
                    $('#facture_table').ajax.load();
                    console.log($('#facture_table'));
                }
            })
        }
        else
        {
            return false;
        }
    });
    });





</script>

@endsection
