<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeUnsigned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `ligne_facture` DROP FOREIGN KEY `fk_facture`');
        DB::statement('ALTER TABLE `ligne_facture` ADD CONSTRAINT `fk_facture` FOREIGN KEY (`ide_facture`) REFERENCES `facture`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ligne_facture', function (Blueprint $table) {
            //
        });
    }
}
