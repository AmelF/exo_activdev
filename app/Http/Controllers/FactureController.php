<?php

namespace App\Http\Controllers;

use App\Facture;
use Barryvdh\Debugbar\Middleware\Debugbar;
use Illuminate\Http\Request;
use App\Lfacture;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class FactureController extends Controller
{
    public function index(\Illuminate\Http\Request $request)
    {
        //Retrieve PDO connection
//        $pdo = DB::connection()->getPdo();
//
//
//        //Retrieve input "filter"
//        $filter = Input::get('filter');
//
//        //Retrieve lines
//        $lines = $pdo->query('SELECT * FROM facture')->fetchAll();
//
//        //Return data to the view


       $facture=Facture::leftJoin('ligne_facture', 'facture.id', '=', 'ligne_facture.ide_facture')
           ->selectRaw('facture.*, SUM(ligne_facture.quantity*ligne_facture.unit_price) as total')
           ->groupBy('facture.id')
           ->paginate(8);

//       print_r($facture);

        return view('factures', compact('facture'));


    }


    /**
     * @param Request $request
     * @return \Illuminate\View\View
     *
     * Created by @AmelSid.
     * Date: 22/01/2019
     * Time: 13:03
     */
    public function search(Request $request)
    {

        $searchNumber = $request->number;
        $searchStatu = strtoupper($request->status);

        if ($searchNumber) {

            $facture = Facture::whereHas('ligne_facture', function ($query) use ($searchStatu, $searchNumber) {
                $query->Where('numero', 'like', $searchNumber);
            })->paginate(4);


            return view('factures', compact('facture'));


        }

        if ($searchStatu) {
            $facture = Facture::whereHas('ligne_facture', function ($query) use ($searchStatu, $searchNumber) {
                $query->where('status', 'like', $searchStatu);

            })->paginate(4);
            return view('factures', compact('facture'));


        }


    }

    public function sortTotal(Request $req)
    {

        switch ($req->sort) {

            case 'desc':
        $facture=Facture::leftJoin('ligne_facture', 'facture.id', '=', 'ligne_facture.ide_facture')
            ->selectRaw('facture.*, SUM(ligne_facture.quantity*ligne_facture.unit_price) as total')
            ->groupBy('facture.id')
            ->orderBy('total', 'ASC')
            ->paginate(8);

        return view('factures', compact('facture'));
                break;

            case 'asc':
                        $facture=Facture::leftJoin('ligne_facture', 'facture.id', '=', 'ligne_facture.ide_facture')
            ->selectRaw('facture.*, SUM(ligne_facture.quantity*ligne_facture.unit_price) as total')
            ->groupBy('facture.id')
            ->orderBy('total', 'DESC')
            ->paginate(8);

        return view('factures', compact('facture'));
                break;
        }



    }



    public function delete($id)
    {

        $fac = Facture::findOrFail($id)->delete();
        ;
        return response()->json(['success' => true], 200);


    }
}
