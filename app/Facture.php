<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Facture extends Model
{

    /**
     * Association de la table avec le model
     *
     *
     * @var string
     */

    protected $table = 'facture';


    protected $fillable = [
        'id',
        'numero',
        'fname',
        'lname',
        'address',
        'city',
        'zipcode',
        'date',
        'status'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * Created by @AmelSid.
     * Date: 22/01/2019
     * Time: 13:12
     */
    public function ligne_facture()
    {

        return $this->hasOne('App\Lfacture','ide_facture');
    }


    public function total()
    {
        return $this->ligne_facture()->selectRaw('unit_price * quantity as total');
    }


    // solution trouvé sur le web

    /**
     *

     */
    public static function boot()
    {
        parent::boot();


        static::deleted(function($facture)
        {
            $facture->ligne_facture()->delete();
        });
    }
}
