<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lfacture extends Model
{
    protected $table = 'ligne_facture';

    protected $fillable = [
        'id',
        'id_facture',
        'reference',
        'unit_price',
        'quantity'

    ];
    public function facture()
    {
        return $this->hasOne('App\facture','id');
    }



}
